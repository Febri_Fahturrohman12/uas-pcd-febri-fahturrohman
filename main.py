# UAS PCD MENDETEKSI OBJEK PERGERAKAN MOBIL
# 20201014 FEBRI FAHTURROHMAN SALEH

# import library opencv yang fungsinya untuk membaca dan menampilkan video
import cv2

# import media video lalulintas
cap = cv2.VideoCapture("trafficroad.mp4")

# Deteksi objek dari kamera stabil
object_detector = cv2.createBackgroundSubtractorMOG2(history=100, varThreshold=40)

# proses pengolahan
while True:
    ret, frame = cap.read()

    # untuk menampilkan height dan width dari video tersebut
    height, width, _ = frame.shape
    # print(height, width)

    # Ekstrak wilayah atau area yang dipilih untuk pendeteksian dari sebuah frame video
    roi = frame[300:720,500:800]

    # Deteksi Objek
    mask = object_detector.apply(roi)

    # penyamaran atau menghilangkan bayangan pada objek yang bergerak untuk mendapatkan objek lebih akurat dengan cara menghilangkan warna selain putih
    _, mask = cv2.threshold(mask, 254, 255, cv2.THRESH_BINARY)

    # penambahan contour untuk objek bergerak pada area yang dipilih
    contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # perulangan contour
    for cnt in contours:
        # Hitung luas dan hapus elemen kecil
        area = cv2.contourArea(cnt)
        if area > 700:
            # mengambar atau menempelkan contour pada objek bergerak
            # cv2.drawContours(roi, (cnt), -1, (0,255,0), 2)

            # penambahan bingkai pada objek bergerak untuk menggantikan contour
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(roi, (x,y), (x+w, y+h), (0, 255, 0), 3)


            
    # menampilkan video frame
    cv2.imshow("Frame", frame)

    # menampilkan video hasil substraction
    # cv2.imshow("Mask", mask)

    # menampilkan video hanya pada area yang dipilih
    # cv2.imshow("Area", roi)

    key = cv2.waitKey(30)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()